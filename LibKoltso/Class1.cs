﻿using System;

namespace LibKoltso
{
    public class Class1
    {
        public const double pi = 3.14;
        public static double Koltso(double R, double r)
        {
            double S = pi * ((R * R) - (r * r));
            return S;
        }
    }
}
