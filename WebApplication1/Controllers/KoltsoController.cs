﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class KoltsoController : Controller
    {
        public IActionResult Koltso([FromQuery] Koltso request)
        {
            request.S = LibKoltso.Class1.Koltso(request.R1, request.r);
            return View(request);
        }
    }
}
